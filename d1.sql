-- To run MySQL/Maria DB

	-- mysql -u root 

	-- -u stands for username
	-- root is the default username for sql
	-- -p stands for password

-- Showing/retrieving all database:
SHOW DATABASES;

-- Commands in SQL will still work with all lowercase letters. 
-- Using all Caps allows for code readability to distinguish commands with table names, column names and value inputs.
-- Make sure semi-colons are added at the end of the syntax.

-- Create a database
-- Syntax: CREATE DATABASE database_name;

CREATE DATABASE music_store;


-- Dropping/deleting a database
-- Syntax: DROP DATABASE database_name;

DROP DATABASE music_store;

-- Recreate our music database

CREATE DATABASE music_db;

-- Select a database:
-- Syntax; USE database_name;

USE music_db;


-- Creating/adding tables:
-- Syntax: 
	-- CREATE TABLE table_name(
	-- 		column_name data_type other_options,
	-- 		column2,
	-- 		PRIMARY KEY (id)
	-- 	);


CREATE TABLE singers (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);


-- SHOWING/RETRIEVING TABLES;

SHOW TABLES;


-- DROPPING/DELETING TABLES;

DROP TABLE singers;


-- Create "artists" table

CREATE TABLE artists (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- Describing tables allows to see the table columns, data types and extra option set.

DESCRIBE artists;


-- Create tables with foreign key
-- Syntax:

	CONSTRAINT foreign_key_name 
		FOREIGN KEY (column_name) REFERENCES table_name(id)
		ON UPDATE ACTION(CASCADE, NO ACTION, SET NULL, SET DEFAULT)
		ON DELETE ACTION(RESTRICT, SET NULL)


CREATE TABLE records (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(25) NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


-- RENAME "records" table to "albums" table.

-- Syntax:
	ALTER TABLE table_name 
		RENAME TO new_table_name;


ALTER TABLE records
	RENAME TO albums;


-- CREATING/ADDING COLUMNS TO A TABLE
-- Syntax: 
	ALTER TABLE table_name
		ADD column_name data_type extra_options;


ALTER TABLE albums
	ADD date_released DATE NOT NULL;


-- DROPPING/DELETING COLUMNS:
-- Syntax: 
	ALTER TABLE table_name
		DROP COLUMN column_name;


ALTER TABLE albums
	DROP COLUMN date_released;


-- Adding a column to a specific position
-- Syntax:
	ALTER TABLE table_name
		ADD column_name data_type extra_options
			AFTER column_name


ALTER TABLE albums
	ADD year DATE NOT NULL
		AFTER album_title;


-- MODIFYING A COLUMN

-- Syntax:
	ALTER TABLE table_name
		MODIFY column_name data_type extra_options

ALTER TABLE albums
	MODIFY album_title VARCHAR(50) NOT NULL;

-- The 'FIRST' and 'AFTER' keywords may also be added to change the position of a column


-- RENAMING A COLUMN:
-- SYNTAX:
	ALTER TABLE table_name
		CHANGE COLUMN old_name new_name data_type extra_options;


ALTER TABLE albums
	CHANGE COLUMN year date_released DATE NOT NULL;

-- Specifying the data_type and extra_options is required even if the column definition do not change

-- Create users table
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id) 
);

-- Create songs table
CREATE TABLE songs (
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULl,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Mini Activity:
-- Create a table named "playlists" with the following columns and constraints:
-- id/INT/ Not Nullable/Auto-increment
-- users_id INT/Not nullable
-- datetime_created/DATETIME/Not nullable
-- Primary key: id
-- Constraint named fk_playlists_user_id that sets the user_id column to reference the users tables id column
-- On update cascade/on delete restrict

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


-- Create playlists_songs table
CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);






